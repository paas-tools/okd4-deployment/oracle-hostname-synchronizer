module okd4-deployment/oracle-hostname-synchronizer

go 1.13

require (
	github.com/godror/godror v0.16.0
	github.com/operator-framework/operator-sdk v0.17.0 // indirect
	github.com/sirupsen/logrus v1.5.0
	gopkg.in/goracle.v2 v2.24.1 // indirect
	k8s.io/apimachinery v0.17.4
	k8s.io/client-go v12.0.0+incompatible
	sigs.k8s.io/controller-runtime v0.5.2
)

replace (
	github.com/Azure/go-autorest => github.com/Azure/go-autorest v13.3.2+incompatible // Required by OLM
	k8s.io/client-go => k8s.io/client-go v0.17.4 // Required by prometheus-operator
)
