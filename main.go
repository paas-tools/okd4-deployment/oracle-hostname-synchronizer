package main

import (
	"crypto/md5"
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"strings"

	"database/sql"

	"okd4-deployment/oracle-hostname-synchronizer/pkg/apis/webservices/v1alpha1"

	_ "github.com/godror/godror"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"

	log "github.com/sirupsen/logrus"
)

const (
	zone = ".web"
)

var (
	kubeconfig       = flag.String("kubeconfig", "", "path to Kubernetes config file")
	connectionString = flag.String("oracle-connection-string", "default", "Connection string to oracle database")
	// When testing the performance of External-DNS we discovered that if we ask OpenShift to create a CR that holds 15000 endpoints
	// the request is considered too large. We picked 10 CRs to make sure there was enough space in each CR and that each one would be more or less 50% full.
	numberOfCrs   = flag.Int("number-of-crs", 10, "Number of CRs that are going to be created to host all the entries in the database")
	namespace     = flag.String("namespace", "paas-infra-dnsmanager", "Namespace where the CRs are going to be created")
	nameOfRecords = flag.String("name", "record", "Name that the CRs will have that will be concatenated with their number e.g record-1, record-2...")
)

func main() {
	// Parses the command line into the defined flags
	flag.Parse()

	// Command line arguments validation
	if *connectionString == "default" {
		log.Fatal("A connection string was not provided, please provide one")
	}

	// Create K8S/OKD client
	client := newClientForConfig(*kubeconfig)
	log.Info("Successfully connected to K8S/OKD")

	// Open connection to DB in connection string
	db, err := sql.Open("godror", *connectionString)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer db.Close()
	log.Info("Successfully connected to Oracle DB")

	log.Info("Going to preform query")
	// Here I thought about using WHERE to filter the rows of TYPE "EOSCONTAINERS" but this removes
	// the rows that have NULL TYPE which we don't want to lose
	rows, err := db.Query("SELECT * FROM \"WEBREG\".\"ZONE_WEB_WITH_TYPE\"")
	if err != nil {
		log.Fatal("Error running query: %v", err)
		return
	}
	defer rows.Close()

	// Filling the zone structure
	// This map will contain as keys the alias that we want to create
	// (e.g. bob, alice, eve) and in the values it will be a list
	// of all targets that we want to point to (e.g. oonodelb.cern.ch).
	// ExternalDNS will then create a DNS CNAME entry for each target in the list of targets.
	endpoints := []string{}
	var alias, name, domain, siteType string
	for rows.Next() {
		if err := rows.Scan(&alias, &name, &domain, &siteType); err != nil {
			log.Fatal(err)
		}
		alias = strings.ToLower(alias)
		domain = strings.ToLower(domain)
		siteType = strings .ToLower(siteType)
		
		// Filter out rows of type eoscontainer as those will be published by the new okd4 infra
		if siteType == "eoscontainer" {
			continue
		}

		endpoints = append(endpoints, alias + zone + domain)
	}

	log.Info("Zone struture built and populated")

	// Generate all the ReservedHostname that we will have to create/update
	rsvHostnames := generateCRs(endpoints)

	// Iterate arry and create/update each resource
	for _, rsvHostname := range rsvHostnames {
		createResource(client, rsvHostname)
	}
}

func generateCRs(zone []string) (rsvHostnames []*v1alpha1.ReservedHostname) {
	var hostnames = make([][]string, *numberOfCrs)

	for _, alias := range zone {
		// We will use the result of MD5Hash(alias) % numberOfCrs to determine in which CR we are going to store this record.
		// We do this to first evenly distribute the different DNS records among the different CRs and to also avoid the problem
		// that if a record is removed we will for sure remove it from the CRs the next time this program runs
		hash := md5.Sum([]byte(alias))
		key := binary.BigEndian.Uint16(hash[:]) % uint16(*numberOfCrs)

		hostnames[key] = append(hostnames[key], alias)
	}

	rsvHostnames = make([]*v1alpha1.ReservedHostname, *numberOfCrs)
	for i := 0; i < *numberOfCrs; i++ {
		name := fmt.Sprintf("%s-%d", *nameOfRecords, i+1)
		rsvHostname := &v1alpha1.ReservedHostname{
			ObjectMeta: v1.ObjectMeta{
				Name: name,
			},
			Spec: v1alpha1.ReservedHostnameSpec{
				Hostnames: hostnames[i],
			},
		}

		rsvHostnames[i] = rsvHostname
	}

	return
}

// Function that will try to create or patch the CRs in openshift using the REST API
func createResource(client *rest.RESTClient, rsvHostname *v1alpha1.ReservedHostname) {

	// Attempts to create the resource
	result := client.Post().
		Namespace(*namespace).
		Resource(v1alpha1.ResourcePlural).
		Body(rsvHostname).
		Do()

	var status int
	result.StatusCode(&status)

	//In case the object already exists lets just patch the Endpoints of the resource
	if status == 409 {

		payload := []patchStringValue{{
			Op:    "replace",
			Path:  "/spec/hostnames",
			Value: rsvHostname.Spec.Hostnames,
		}}

		toSend, err := json.Marshal(payload)
		if err != nil {
			panic(err)
		}

		result = client.Patch(types.JSONPatchType).
			Namespace(*namespace).
			Resource(v1alpha1.ResourcePlural).
			Name(rsvHostname.ObjectMeta.Name).
			Body(toSend).
			Do()
		result.StatusCode(&status)
	}
	// Final result of the operation
	if status != 201 && status != 200 {
		log.Fatal(result.Error().Error())
	}
	log.Info("Created/Updated CR ReservedHostname named ", rsvHostname.ObjectMeta.Name, " with ", len(rsvHostname.Spec.Hostnames), " endpoints")
}
