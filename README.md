# Oracle Hostname Synchronizer

This is a simple golang script that is responsible by querying an Oracle database (`cerndb1`) to fetch all the hostnames currently being used by the old webeos infrastructure (view `ZONE_WEB`) then, it will create/update a set of CRs of kind `ReservedHostname` with the result of that query.

## Parametrized values
In order to interact with this parametrized values, the only requirement is to add the pertinent flag during the execution (e.g. ```-number-of-crs 5```)

**Mandatory values**:
- **oracle-connection-string**: (e.g ```bob/password@pdb-s.cern.ch:10121/PDB_CERNDB1.cern.ch```). This values is the oracle connection string that will allow the program to connect himself to the Oracle database;

Optional values:

- **kubeconfig**: Path to the kubeconfig file, only necessary if running outside the cluster
- **number-of-crs**: (e.g ```5```) Default value is ```10```. This value is total number of CRs that will be created to hold the mutiple DNS entries among them. Entries are assigned to a a CR using the ```MD5Hash(alias) % number-of-crs```;
- **namespace**: (e.g ```myproject``` ) Default value is ```paas-infra-dnsmanager```;
- **name**: (e.g ```fancy-name```) Default value is ```record```. This will be the prefix of the name of each CR created concatenated with their number (e.g record-1, record-2...)
