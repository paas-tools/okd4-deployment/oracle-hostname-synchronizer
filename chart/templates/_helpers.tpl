{{/*
This set up doesn't support strings with special characters out of the box
in case this is necessary reffer to https://stackoverflow.com/a/3177871.
NB: Special character either in the username or in the password are not recommended
even by the database team.
*/}}

{{- define "oracle-table-sync.connection-string" -}}
{{- printf "%s/%s@%s" .Values.oracle.username .Values.oracle.password .Values.oracle.database | b64enc -}}
{{- end -}}
