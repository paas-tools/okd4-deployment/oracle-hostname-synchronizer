package main

import (
	"log"
	"okd4-deployment/oracle-hostname-synchronizer/pkg/apis/webservices/v1alpha1"

	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

// Type necessary to hold all the group flags that might be in the parameters
type arrayFlags []string

// Necessary function to be implemented
func (i *arrayFlags) String() string {
	return "my string representation"
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

//  patchStringValue specifies a patch operation for a string.
type patchStringValue struct {
	Op    string `json:"op"`
	Path  string `json:"path"`
	Value []string `json:"value"`
}


func newClientForConfig(kubeconfig string) *rest.RESTClient {
	var err error
	var config *rest.Config

	// creates cluster config
	if kubeconfig == "" {
		log.Printf("Using in-cluster configuration")
		config, err = rest.InClusterConfig()
	} else {
		log.Printf("Using configuration from '%s'", kubeconfig)
		config, err = clientcmd.BuildConfigFromFlags("", kubeconfig)
	}
	if err != nil {
		panic(err.Error())
	}

	// Registering in the config the schema od the CRD
	v1alpha1.SchemeBuilder.AddToScheme(scheme.Scheme)

	// Setting up the group and version for client config
	config.ContentConfig.GroupVersion = &v1alpha1.SchemeGroupVersion
	config.APIPath = "/apis"
	config.NegotiatedSerializer = scheme.Codecs.WithoutConversion()
	config.UserAgent = rest.DefaultKubernetesUserAgent()

	// Creates the clientset
	clientset, err := rest.UnversionedRESTClientFor(config)
	if err != nil {
		panic(err.Error())
	}
	return clientset
}
