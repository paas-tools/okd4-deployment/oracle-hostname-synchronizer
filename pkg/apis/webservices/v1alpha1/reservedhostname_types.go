package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const ResourcePlural = "reservedhostnames"

// ReservedHostnameSpec defines the desired state of ReservedHostname
type ReservedHostnameSpec struct {
	// +kubebuilder:validation:Required
	//Hostnames array of hostnames currently being used by the old infrastructure
	Hostnames []string `json:"hostnames,omitempty"`
}

// ReservedHostnameStatus defines the observed state of ReservedHostname
type ReservedHostnameStatus struct {
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ReservedHostname is the Schema for the reservedhostnames API
// +kubebuilder:subresource:status
// +kubebuilder:resource:path=reservedhostnames,scope=Namespaced
type ReservedHostname struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ReservedHostnameSpec   `json:"spec,omitempty"`
	Status ReservedHostnameStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ReservedHostnameList contains a list of ReservedHostname
type ReservedHostnameList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ReservedHostname `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ReservedHostname{}, &ReservedHostnameList{})
}
