FROM golang:1.13.0 AS builder

# Copy the code from the host and compile it
WORKDIR $GOPATH/src/gitlab.cern.ch/paas-tools/networking/oracle-table-synchronizer
COPY . ./
RUN CGO_ENABLED=1 GOOS=linux go build -a -installsuffix nocgo -mod vendor -o /main .

FROM oraclelinux:7-slim

ARG release=19
ARG update=3

RUN  yum -y install oracle-release-el7 && yum-config-manager --enable ol7_oracle_instantclient && \
     yum -y install oracle-instantclient${release}.${update}-basic oracle-instantclient${release}.${update}-devel oracle-instantclient${release}.${update}-sqlplus && \
     rm -rf /var/cache/yum

COPY --from=builder /main ./
ENTRYPOINT ["./main"]
